//
//  NetworkManager.swift
//  POC-Weather
//
//  Created by Jhonathan Sanchez on 3/5/19.
//  Copyright © 2019 Jhonathan Sanchez. All rights reserved.
//

import Foundation

enum WeatherResult {
    case success([Weather])
    case imageSuccess(Data)
    case error(String)
}

class NetworkManager {
    private let session = URLSession(configuration: .default)
    enum EndpointURL: String {
        case serviceURL = "https://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22"
        case imgURL = "http://openweathermap.org/img/w/"
    }
    
    func fetchCurrentWeather(completion: @escaping(_ weatherDetails: WeatherResult) -> Void) {
        guard let url = URL(string: EndpointURL.serviceURL.rawValue) else { return }
        
        session.dataTask(with: url) { (data, response, error) in
            if error != nil {
                completion(.error(error?.localizedDescription ?? "Error Fetching Weather"))
                return
            }
            
            if let data = data {
                let decoder = JSONDecoder()
                do {
                    let weatherWrapper = try decoder.decode(CurrentWeather.self, from: data)
                    let weather = weatherWrapper.weather
                    completion(.success(weather))
                } catch let err {
                    completion(.error(err.localizedDescription))
                    return
                }
            }
        }.resume()
    }
    
    func fetchIconImage(for iconName: String, completion: @escaping(_ imageData: WeatherResult) -> Void) {
        let urlStr = "\(EndpointURL.imgURL.rawValue)\(iconName).png"
        guard let url = URL(string: urlStr) else { return }
        session.dataTask(with: url) { (data, response, error) in
            if error != nil {
                completion(.error(error?.localizedDescription ?? "Error geting image"))
                return
            }
            
            if let data = data {
                completion(.imageSuccess(data))
            }
        }.resume()
    }
}
