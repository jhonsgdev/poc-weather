//
//  CurrentWeather.swift
//  POC-Weather
//
//  Created by Jhonathan Sanchez on 3/5/19.
//  Copyright © 2019 Jhonathan Sanchez. All rights reserved.
//

import Foundation

struct CurrentWeather: Codable {
    let weather: [Weather]
}

struct Weather: Codable {
    let main: String
    let icon: String
}
