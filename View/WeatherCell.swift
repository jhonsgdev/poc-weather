//
//  WeatherCell.swift
//  POC-Weather
//
//  Created by Jhonathan Sanchez on 3/5/19.
//  Copyright © 2019 Jhonathan Sanchez. All rights reserved.
//

import UIKit

class WeatherCell: UITableViewCell {

    @IBOutlet weak var mainDescriptionLabel: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(weather: Weather?) {
        guard let londonWeather = weather, weather != nil else { return }
        
        mainDescriptionLabel.text = londonWeather.main
        let networkManager = NetworkManager()

        networkManager.fetchIconImage(for: londonWeather.icon) { [weak self] (result) in
            if let weakSelf = self {
                DispatchQueue.main.async {
                    if case let .imageSuccess(imageData) = result {
                        let icon = UIImage(data: imageData)
                        weakSelf.iconView.image = icon
                    }
                    
                    if case let .error(error) = result {
                        //set placeholder image
                        print(error)
                    }
                }
            }
        }
    }
}
