//
//  ViewController.swift
//  POC-Weather
//
//  Created by Jhonathan Sanchez on 3/5/19.
//  Copyright © 2019 Jhonathan Sanchez. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    private var weather: [Weather]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        let networkManager = NetworkManager()
        networkManager.fetchCurrentWeather { [weak self] (result) in
            if let weakSelf = self {
                DispatchQueue.main.async {
                   weakSelf.handleResult(result: result)
                }
            }
        }
    }
    
    private func handleResult(result: WeatherResult) {
        if case let .error(error) = result {
            print(error)
            let alert = UIAlertController(title: error, message: error, preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .default) { (action) in
                //handle action
            }
            
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
        }
        
        if case let .success(weatherResult) = result {
            weather = weatherResult
            tableView.reloadData()
        }
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weather?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? WeatherCell else {
            return UITableViewCell()
        }
        
        let londonweather = weather?[indexPath.row]
        cell.configureCell(weather: londonweather)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
}

